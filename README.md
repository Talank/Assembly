Assignment 2 
Question Set
--------------------------------------------------------------------------------------------------------------
1. Write an Assembly language program to
a) Sum three numbers stored at 80h, 81h, 82h
    SOLUTION: sumOfThreeNumbers.asm
    
b) (swap three numbers) store the value at 80h<--81h , 81h<--82h,82h<--8h 
    SOLUTION: swapThreeNumbers.asm
    
d) Store the square of the number stored at 80h into 81h ( i.e if 80h has 8, 81h should store 64)
    SOLUTION: findSquare.asm
    
c) sum individual digits of a number store at 80h and store the sum into 81h
	Example if value at 80h is 13, 1+3 = 4 should be stored at 81h
	Hint: use rotate 
	SOLUTION: sumOfDigits.asm
	
e) Divide a number by 10 store into 81h
    SOLUTION: divideBy10.asm

f) find the smallest number from a list of numbers stored at address
    SOLUTION: smallestNumber.asm
    
---------------------------------------------------------------------------------------------------------------